package avalle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class Main {

    public static void main(String[] args) throws IOException {

        //Write a program that incorporates a list, a queue, a set, and a tree
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int ID = 0;
        String Name;
        String lastName;
        String Answer;

        Boolean loop = true;

        // I will use this list to store the students that the user add. Then it will be used to add values to the others Collections
        List<Student> list = new ArrayList<Student>();

        // it is important to add at least 4 student and repeat information to see the differences on the collections.
        System.out.print("Please add a least 4 students\n");

        while(loop){

            System.out.print("Add a new student\n");

            System.out.print("Enter a number for the ID: ");
            // here I handle exception if the ID is not a number
            try {
               ID = Integer.parseInt(br.readLine());
                System.out.print("Enter Student Name: ");
                Name = br.readLine();

                System.out.print("Enter Student Last Name: ");
                lastName = br.readLine();

                list.add(new Student(ID,Name,lastName));

                System.out.print("Do you want to add a new Student?  Enter YES / NO: ");
                Answer = br.readLine();
                if(Answer.toLowerCase().equals("no")){
                    loop = false;
                }
            } catch(NumberFormatException nfe) {
                System.out.println("Please enter a Number!");
            }


        }

        // It will Store duplicate elements, and it will maintain the insertion order.
        System.out.println("### Using list ###");

        for(Student student : list){
            System.out.println(student);
        }

        //Queue will maintains the first in first out
        System.out.println();
        System.out.println("### Using Queue ###");
       Queue<Student> queue = new ArrayDeque<Student>();
      // will add elements to the queue
       for(Student student : list){
           queue.add(student);
       }
       // will display the data
        for(Student student : queue){
            System.out.println(student);
        }

        // it will remove the first to elements to show how Queue works.
        queue.remove();
        queue.poll();

        System.out.println();
        System.out.println("### QUEUE -- After Removing Elements ###");

        //will display the elements on the queue after removing elements.
        for(Student student : queue){
            System.out.println(student);
        }


        System.out.println();
        System.out.println("### Using Tree - Comparing Last Name ###");
        //The treeSet can have unique elements. I implemented a comparator interface where I am asking for the Last Name.
        // It will store unique Last Name and sort it by last name.
        TreeSet <Student> tree = new TreeSet<Student>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
               return o1.getLastName().compareTo(o2.getLastName());
            }
        });
        // will add elements from the list to the tree
        for(Student student : list){
            tree.add(student);
        }
        // will display elements on the tree
        Iterator iteratorT = tree.iterator();
        while (iteratorT.hasNext()){
            System.out.println(iteratorT.next());
        }

        System.out.println();
        System.out.println("### Using Set - has unique unordered elements. ###");
        // will store unique elements from the list
        Set<String> set = new HashSet<String>();
        for(Student student : list){
            set.add(student.getFirstName());
        }

        // will display elements.
        Iterator iteratorSet = set.iterator();
        while (iteratorSet.hasNext()){
            System.out.println(iteratorSet.next());
        }

    }
}
